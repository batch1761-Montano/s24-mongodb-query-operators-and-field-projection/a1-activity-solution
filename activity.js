db.products.insertMany([
    {
        "name": "Iphone X",
        "price": 30000,
        "isActive": true    
    },
    {
        "name": "Samsung Galaxy S21",
        "price": 51000,
        "isActive": true    
    },
    {
        "name": "Razer Blackshark V2X",
        "price": 2800,
        "isActive": false    
    },
    {
        "name": "RAKK Gaming Mouse",
        "price": 1800,
        "isActive": true    
    },
    {
        "name": "Razer Mechanical Keyboard",
        "price": 4000,
        "isActive": true    
    },
])
 
db.users.insertMany([
        {
            "firstName":"Mary Jane",
            "lastName":"Watson",
            "email":"mjtiger@gmail.com",
            "password":"tigerjackpot15",
            "isAdmin":false
        },
        {
            "firstName":"Gwen",
            "lastName":"Stacy",
            "email":"stacyTech@gmail.com",
            "password":"stacyTech1991",
            "isAdmin":true
        },
        {
            "firstName":"Peter",
            "lastName":"Parker",
            "email":"peterWebDev@gmail.com",
            "password":"webdeveloperPeter",
            "isAdmin":true
        },
        {
            "firstName":"Jonah",
            "lastName":"Jameson",
            "email":"jjjameson@gmail.com",
            "password":"spideyisamenace",
            "isAdmin":false
        },
        {
            "firstName":"Otto",
            "lastName":"Octavius",
            "email":"ottoOctopi@gmail.com",
            "password":"docOck15",
            "isAdmin":true
        },
]) 
  
//Activity Instructions (Query-176 db)    

//Find users with letter 'y' in their firstname or last name
//  - show only their email and isAdmin properties/fields
  db.users.find({$or:[{firstName:{$regex:'y',$options:'$i'}},{lastName:{regex:'y',$options:'$i'}}]},{"_id":0,"email":1,"isAdmin":1})  
//Find users with letter 'e' in their firstname and is an admin.

    // - show only their email and isAdmin properties/fields.

 db.users.find({$and:[{firstName:{$regex:'e',$options:'$i'}},{isAdmin:true}]},{"_id":0,"email":1,"isAdmin":1})

//Find products with letter x in the its name and has a price greater than or equal to 50000

  db.products.find({$and:[{name:{$regex:'x',$options:'$i'}},{price:{$gte:50000}}]})

//Update all products with price less than 2000 to inactive  

  db.products.updateMany({price:{$lt:2000}},{$set:{"isActive": false}})

//Delete all products with price greater than 20000.

  db.products.deleteMany({price:{$gt:20000}}) 

  
